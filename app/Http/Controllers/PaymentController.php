<?php

namespace App\Http\Controllers;

use App\Helpers\PaypalHelper;
use App\Models\PaymentService;
use App\Models\TicketType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalHttp\HttpException;

class PaymentController extends Controller
{


    /**
     * Create
     *
     * This endpoint is used to create a payment and generating the ticket
     *
     * @group Payment
     *
     * @bodyParam type string required the payment type
     * @bodyParam event_id string required the event id
     * @bodyParam ticket_type_id string required the ticket type id
     * @bodyParam email string required the email
     *
     * @response {
     *  "status": "success",
     *  "message": "The order has been successfully created."
     * }
     * */
    public function create(Request $req){

        $rules = [
            "type" => "required",
            "event_id" => "required|exists:events,id",
            "ticket_type_id" => "required|exists:ticket_types,id",
            "email" => "required|email"
        ];

        $validator = Validator::make($req->post(), $rules);

        if($validator->fails()){
            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $ticketType = TicketType::Where([["id", $req->post("ticket_type_id")], ["event_id", $req->post("event_id")]])->with("tickets")->with("event");

        if(!$ticketType->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No ticket type with this id, was found for this event"
            ], 404);
        }

        $ticketType = $ticketType->first();

        if(count($ticketType->tickets) >= $ticketType->available){
            return response()->json([
                "status" => "error",
                "message" => "All of this ticket type has already been sold out."
            ], 400);
        }


        switch (strtoupper($req->post("type"))){

            case "PAYPAL":
                $paypayHelper = new PaypalHelper();
                return $paypayHelper->createPayment($ticketType->event->company_id, $ticketType->price, $req->post("ticket_type_id"),  $req->post("email"));

            default:
                return response()->json([
                    "status" => "error",
                    "message" => "Payment method was not found"
                ], 404);
        }

    }

    /**
     * Verify
     *
     * This endpoint is used to verify a payment and generating the ticket
     *
     * @group Payment
     *
     * @bodyParam type string required the payment type
     * @bodyParam order_id string required the order id
     *
     * @response {
     *  "status": "success",
     *  "message": "The order has been successfully verified."
     * }
     * */
    public function verify(Request $req){

        $rules = [
            "type" => "required",
            "order_id" => "required",
        ];

        $validator = Validator::make($req->post(), $rules);

        if($validator->fails()){
            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        switch (strtoupper($req->post("type"))){

            case "PAYPAL":
                $paypayHelper = new PaypalHelper();
                return $paypayHelper->verify($req->post("order_id"));

            default:
                return response()->json([
                    "status" => "error",
                    "message" => "Payment method was not found"
                ], 404);
        }

    }
}
