<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->char("id", 36)->nullable(false)->primary();
            $table->char("company_id", 36)->nullable(false);
            $table->char("account_id", 36)->nullable(false);
            $table->string("title", 45)->nullable(false);
            $table->string("image_path")->nullable(true);
            $table->string("desc")->nullable(false);
            $table->integer("address_id")->nullable(false);
            $table->bigInteger("created")->nullable(false);
            $table->bigInteger("date")->nullable(false);

            $table->foreign("company_id")->references("id")->on("companies");
            $table->foreign("account_id")->references("id")->on("accounts");
            $table->foreign("address_id")->references("id")->on("addresses");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
