# Payment


## Verify


This endpoint is used to verify a payment and generating the ticket

> Example request:

```bash
curl -X POST \
    "http://localhost/payments/verify" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"type":"eos","orderid":"id"}'

```

```javascript
const url = new URL(
    "http://localhost/payments/verify"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "type": "eos",
    "orderid": "id"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/payments/verify',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'json' => [
            'type' => 'eos',
            'orderid' => 'id',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/payments/verify'
payload = {
    "type": "eos",
    "orderid": "id"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The order has been successfully verified."
}
```
<div id="execution-results-POSTpayments-verify" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTpayments-verify"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTpayments-verify"></code></pre>
</div>
<div id="execution-error-POSTpayments-verify" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTpayments-verify"></code></pre>
</div>
<form id="form-POSTpayments-verify" data-method="POST" data-path="payments/verify" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTpayments-verify', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTpayments-verify" onclick="tryItOut('POSTpayments-verify');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTpayments-verify" onclick="cancelTryOut('POSTpayments-verify');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTpayments-verify" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>payments/verify</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>type</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="type" data-endpoint="POSTpayments-verify" data-component="body" required  hidden>
<br>
the payment type
</p>
<p>
<b><code>orderid</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="orderid" data-endpoint="POSTpayments-verify" data-component="body" required  hidden>
<br>
the order id
</p>

</form>



