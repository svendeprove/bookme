<?php

namespace App\Http\Controllers;

use App\Helpers\PaypalHelper;
use App\Models\Company;
use App\Models\CompanyMember;
use App\Models\CompanyRole;
use App\Models\PaymentService;
use App\Models\PaymentType;
use App\Models\TicketType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class CompanyController extends Controller
{

    /**
     * Create an company
     *
     * This endpoint is used to create a company
     * Role required:
     * Admin
     *
     * @group Company
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam name string required the name of the company
     * @bodyParam account_id string required the the id of the company admin
     * @bodyParam logo the logo image of the company used on the tickets
     *
     * @response {
     *  "status": "success",
     *  "message": "The company was created successfully"
     * }
     * */
    public function create(Request $request){

        $rules = [
            "name" => "required|max:45",
            "account_id" => "required|max:36|exists:accounts,id"
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $companyId = Uuid::uuid4();

        $newCompany = new Company();
        $newCompany->id = $companyId;
        $newCompany->name = $request->post("name");

        if(($file = $request->file("logo")) != null){

            if(strtolower($file->getClientOriginalExtension()) == "png" || strtolower($file->getClientOriginalExtension()) == "jpg" || strtolower($file->getClientOriginalExtension()) == "jpeg") {

                if($file->getSize() < 1000000 && $file->isValid()){
                    $name = bin2hex(openssl_random_pseudo_bytes(10)). ".". $file->getClientOriginalExtension();
                    $path = "data/companies/". $companyId. "/images";
                    $file->move($path, $name);

                    $newCompany->image_path = $path. "/". $name;
                }
            }
        }

        $newCompany->save();

        $companyAdmin = new CompanyMember();
        $companyAdmin->company_id = $companyId;
        $companyAdmin->account_id = $request->post("account_id");
        $companyAdmin->role_id = CompanyRole::where("name", "admin")->first()->id;
        $companyAdmin->save();

        return response()->json([
            "status" => "success",
            "message" => "The company was created successfully"
        ]);
    }


    /**
     * Update a company
     *
     * This endpoint is used to update an event
     * Role required:
     * Company Admin
     *
     * @group Company
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam company_id string required the id of the company
     *
     * @bodyParam name string the name of the company
     * @bodyParam logo the logo image of the company used on the tickets
     *
     * @response {
     *  "status": "success",
     *  "message": "The company has been updated successfully"
     * }
     * */
    public function update(Request $request){

        $rules = [
            "company_id" => "required|max:36|exists:companies,id",
            "name" => "max:100",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $updateValues = [];

        $getCompany = Company::Where([["id", $request->post("company_id")]]);

        if(!$getCompany->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No company where found!"
            ], 404);
        }

        //Updating values
        $paramNames = array("name");
        foreach ($paramNames as $paramName){

            if(($param = $request->post($paramName)) != null){
                $updateValues[$paramName] = $param;
            }
        }

        if(($file = $request->file("logo")) != null){

            if(strtolower($file->getClientOriginalExtension()) == "png" || strtolower($file->getClientOriginalExtension()) == "jpg" || strtolower($file->getClientOriginalExtension()) == "jpeg") {

                if($file->getSize() < 1000000 && $file->isValid()){
                    $name = bin2hex(openssl_random_pseudo_bytes(10)). ".". $file->getClientOriginalExtension();
                    $path = "data/companies/". $request->post("company_id"). "/images";
                    $file->move($path, $name);

                    $updateValues["image_path"] = $path. "/". $name;

                    if(($imagePath = $getCompany->first()->image_path) != null)
                        unlink($imagePath);
                }
            }
        }

        if(count($updateValues) > 0)
            $getCompany->update($updateValues);

        return response()->json([
            "status" => "success",
            "message" => "The company has been updated successfully"
        ]);

    }

    /**
     * Get info about a company
     *
     * This endpoint is used to get the information about a company
     *
     * @group Company
     *
     * @response {
     *  "status": "success",
     *  "message": "Successfully found the company",
     *  "company": {
     *      "id": "51bcf94c-6777-4038-9ad5-87b88b8373b9",
     *      "name": "BookME2",
     *"     image_path": "data/companies/51bcf94c-6777-4038-9ad5-87b88b8373b9/images/6ccae0c200f188797431.png",
     *      "members": [
     *      {
     *          "id": "e580d248-3dd9-460c-bb1e-9ad3a40d7363",
     *          "firstName": "Admin",
     *          "lastName": "Admin",
     *          "role_id": 1,
     *      }
     *  ]
     *}
     * }
     * */
    public function get($id){

        $company = Company::where("id", $id)->with("members");

        if(!$company->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No company with this id exists!"
            ], 404);
        }

        return response()->json([
            "status" => "success",
            "message" => "Successfully found the company",
            "company" => $company->first()
        ]);

    }

    /**
     * Add a payment method to the company
     *
     * This endpoint is used to add a payment method to the company
     * Role required:
     * Admin
     * Company Admin
     *
     * @group Company
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam company_id string required the id of the company
     * @bodyParam payment_type integer required the id of the payment type
     * @bodyParam client_token string required the client token
     * @bodyParam secret_token string required the secret token
     *
     * @response {
     *  "status": "success",
     *  "message": "Successfully added the payment method to the company",
     * }
     * */
    public function addPaymentService(Request $request){

        $rules = [
            "company_id" => "required|max:36|exists:companies,id",
            "payment_type" => "required|integer|exists:payment_types,id",
            "client_token" => "required",
            "secret_token" => "required"
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $paymentType = PaymentType::where("id", $request->post("payment_type"))->first();

        if(PaymentService::where([["payment_type_id", $paymentType->id], ["company_id", $request->post("company_id")]])->exists()){
            return response()->json([
                "status" => "error",
                "message" => "This payment method has already been added",
            ], 400);
        }

        //Validation
        switch ($paymentType->name){
            case "PAYPAL":
                $paypalHelper = new PaypalHelper();
                return $paypalHelper->validateAuth($paymentType->id, $request->post("company_id"), $request->post("client_token"), $request->post("secret_token"));
        }

    }

    /**
     * Remove a payment method from the company
     *
     * This endpoint is used to remove a payment method from the company
     * Role required:
     * Admin
     * Company Admin
     *
     * @group Company
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam company_id string required the id of the company
     * @bodyParam payment_type integer required the id of the payment type
     *
     * @response {
     *  "status": "success",
     *  "message": "Successfully removed the payment method from the company",
     * }
     * */
    public function removePaymentService(Request $request){

        $rules = [
            "company_id" => "required|max:36|exists:companies,id",
            "payment_type" => "required|integer|exists:payment_types,id",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $paymentService = PaymentService::where([["payment_type_id", $request->post("payment_type")], ["company_id", $request->post("company_id")]]);

        if(!$paymentService->exists()) {
            return response()->json([
                "status" => "error",
                "message" => "The company has not added this payment method, so nothing to remove",
            ], 400);
        }

        $paymentService->first()->delete();

        return response()->json([
            "status" => "success",
            "message" => "Successfully removed the payment method from the company",
        ]);

    }

    /**
     * Add a member to the company
     *
     * This endpoint is used to add members to the company
     * Role required:
     * Admin
     * Company Admin
     * Company Manager
     *
     * @group Company
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam company_id string required the id of the company
     * @bodyParam account_id string required the id of the user
     * @bodyParam role_id integer the id of the role
     *
     * @response {
     *  "status": "success",
     *  "message": "The member has been successfully added to the company",
     * }
     * */
    public function addMember(Request $request){

        $rules = [
            "company_id" => "required|max:36|exists:companies,id",
            "account_id" => "required|max:36|exists:accounts,id",
            "role_id" => "integer|exists:company_roles,id",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        if(CompanyMember::where([["company_id", $request->post("company_id")], ["account_id", $request->post("account_id")]])->exists()){
            return response()->json([
                "status" => "error",
                "message" => "The user is already added to the company"
            ], 400);
        }

        $userRole = $request->get("currentUserRole");

        if($request->post("role_id") != null) {

            if ($userRole >= $request->post("role_id")  && !auth()->user()->hasRole("Admin")) {
                return response()->json([
                    "status" => "error",
                    "message" => "You are not allowed to give/set role that is the same or higher than your own"
                ], 401);
            }else{
                $roleId = $request->post("role_id");
            }
        }else{
            $roleId = CompanyRole::where("name", "Staff")->first()->id;
        }

        $addCompanyMember = new CompanyMember();
        $addCompanyMember->company_id = $request->post("company_id");
        $addCompanyMember->account_id = $request->post("account_id");
        $addCompanyMember->role_id = $roleId;
        $addCompanyMember->save();

        return response()->json([
            "status" => "success",
            "message" => "The member has been successfully added to the company"
        ]);

    }

    /**
     * Remove a member from the company
     *
     * This endpoint is used to remove members from the company
     * Role required:
     * Admin
     * Company Admin
     * Company Manager
     *
     * @group Company
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam company_id string required the id of the company
     * @bodyParam account_id string required the id of the user
     *
     * @response {
     *  "status": "success",
     *  "message": "The member has been successfully remove from the company",
     * }
     * */
    public function removeMember(Request $request){

        $rules = [
            "company_id" => "required|max:36|exists:companies,id",
            "account_id" => "required|max:36|exists:accounts,id",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $companyMember = CompanyMember::where([["company_id", $request->post("company_id")], ["account_id", $request->post("account_id")]]);

        if(!$companyMember->exists()){
            return response()->json([
                "status" => "error",
                "message" => "The user is not in the company"
            ], 400);
        }
        $companyMember = $companyMember->first();

        $userRole = $request->get("currentUserRole");


        if ($userRole >= $companyMember->role_id  && !auth()->user()->hasRole("Admin")) {
            return response()->json([
                "status" => "error",
                "message" => "You are not allowed to remove someone that has the same or higher role than your own"
            ], 401);
        }

        $companyMember->delete();

        return response()->json([
            "status" => "success",
            "message" => "The member has been successfully remove from the company"
        ]);

    }

    /**
     * Update a member from the company
     *
     * This endpoint is used to update members from the company
     * Role required:
     * Admin
     * Company Admin
     * Company Manager
     *
     * @group Company
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam company_id string required the id of the company
     * @bodyParam account_id string required the id of the user
     * @bodyParam role_id integer required the id of the role
     *
     * @response {
     *  "status": "success",
     *  "message": "The member has been successfully updated",
     * }
     * */
    public function updateMemeber(Request $request){

        $rules = [
            "company_id" => "required|max:36|exists:companies,id",
            "account_id" => "required|max:36|exists:accounts,id",
            "role_id" => "required|integer|exists:company_roles,id",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $companyMember = CompanyMember::where([["company_id", $request->post("company_id")], ["account_id", $request->post("account_id")]]);

        if(!$companyMember->exists()){
            return response()->json([
                "status" => "error",
                "message" => "The user is not in the company"
            ], 400);
        }

        $userRole = $request->get("currentUserRole");


        if ($userRole >= $companyMember->first()->role_id  && !auth()->user()->hasRole("Admin")) {
            return response()->json([
                "status" => "error",
                "message" => "You are not allowed to modify someone that has the same or higher role than your own"
            ], 401);
        }

        if($userRole >= $request->post("role_id") && !auth()->user()->hasRole("Admin")){
            return response()->json([
                "status" => "error",
                "message" => "You are not allowed to set roles that is the same or higher than yours"
            ], 401);
        }

        $companyMember->update(["role_id" => $request->post("role_id")]);

        return response()->json([
            "status" => "success",
            "message" => "The member has been successfully updated"
        ]);

    }
}
