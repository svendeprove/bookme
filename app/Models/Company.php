<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    public function members(){
        //return $this->hasManyThrough(Company::class, CompanyMember::class, "account_id", "id", "id", "company_id")
        return $this->hasManyThrough(Account::class, CompanyMember::class, "company_id", "id", "id", "account_id")->select(["accounts.id", "accounts.firstName", "accounts.lastName", "company_members.role_id"]);
    }

}
