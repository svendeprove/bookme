# TicketStatus


## Get all Ticket statuses


This endpoint is used to get all the ticket statuses

> Example request:

```bash
curl -X GET \
    -G "http://localhost/ticket/statuses?event_id=vero" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/ticket/statuses"
);

let params = {
    "event_id": "vero",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/ticket/statuses',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'query' => [
            'event_id'=> 'vero',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/ticket/statuses'
params = {
  'event_id': 'vero',
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

response = requests.request('GET', url, headers=headers, params=params)
response.json()
```


> Example response (200):

```json

{
 "status": "success",
 "message": "The ticket statuses has been fetched successfully",
 "ticket_statuses": [
  {
     "id": 1,
     "name": "SOLD"
 },
]
}
```
<div id="execution-results-GETticket-statuses" hidden>
    <blockquote>Received response<span id="execution-response-status-GETticket-statuses"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETticket-statuses"></code></pre>
</div>
<div id="execution-error-GETticket-statuses" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETticket-statuses"></code></pre>
</div>
<form id="form-GETticket-statuses" data-method="GET" data-path="ticket/statuses" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETticket-statuses', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETticket-statuses" onclick="tryItOut('GETticket-statuses');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETticket-statuses" onclick="cancelTryOut('GETticket-statuses');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETticket-statuses" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>ticket/statuses</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>event_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="event_id" data-endpoint="GETticket-statuses" data-component="query" required  hidden>
<br>
the id of the event
</p>
</form>



