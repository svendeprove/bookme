<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->char("id", 36)->nullable(false)->primary();
            $table->string("random_number", 20);
            $table->integer("ticket_type_id")->nullable(false);
            $table->string("email", 255)->nullable(true);
            $table->double("sold_price")->nullable(false);
            $table->bigInteger("date")->nullable(false);
            $table->string("order_id", 45)->nullable(true)->unique();
            $table->string("payment_type", 45)->nullable(false);
            $table->integer("status_id")->nullable(false);

            $table->foreign("ticket_type_id")->references("id")->on("ticket_types");
            $table->foreign("status_id")->references("id")->on("ticket_statuses");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
