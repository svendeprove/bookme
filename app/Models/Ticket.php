<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    public function account(){
        return $this->hasOne(Account::class, "email", "email");
    }

    public function ticketType(){
        return $this->hasOne(TicketType::class, "id", "ticket_type_id");
    }

    public function event()
    {
        return $this->hasOneThrough(Event::class, TicketType::class, "id", "id", "ticket_type_id", "event_id");
    }

}
