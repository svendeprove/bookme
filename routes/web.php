<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

/*
$router->get('/', function () use ($router) {


    return "<html>
  <head>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <!-- Ensures optimal rendering on mobile devices. -->
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /> <!-- Optimal Internet Explorer compatibility -->
  </head>

  <body>
    <script src=\"https://www.paypal.com/sdk/js?client-id=ASYMWLf69U7OH6DcfyWTIxLdf-LiXGbvGhk4Wdb7WxPDQ7bkPH6pSBcOivGq6G0Y0KYUN-7q4c9nShQf&currency=DKK\"> // Replace YOUR_CLIENT_ID with your sandbox client ID
    </script>

    <div id=\"paypal-button-container\"></div>

    <!-- Add the checkout buttons, set up the order and approve the order -->
    <script>
      paypal.Buttons({
          createOrder: function() {
            return fetch('/api/payments/create', {
                method: 'post',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify({
                    type: 'PAYPAL',
                    event_id: '6ef3d7b8-3f6b-44d9-8ac7-28f05521c840',
                    ticket_type_id: 1,
                    email: 'frederikh1011@gmail.com'
                })
            }).then(function(res) {
                return res.json();
            }).then(function(data) {
                return data.order.result.id; // Use the key sent by your server's response, ex. 'id' or 'token'
            });
          },

        onApprove: function(data) {
            return fetch('/api//payments/verify', {
                method: 'post',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify({
                    type: 'PAYPAL',
                    order_id: data.orderID
                })
            }).then(function(res) {
                return res.json();
            }).then(function(details) {
                alert('Transaction approved by ' + details.payer_given_name);
            })
        }

      }).render('#paypal-button-container'); // Display payment options on your web page
    </script>
  </body>
</html>";
});
*/

//Payment controller (only 1 endpoint)
Route::post('payments/create', 'PaymentController@create');
Route::post('payments/verify', 'PaymentController@verify');

//Account controller
Route::post('account/login', 'AccountController@login');
Route::post('account/register', 'AccountController@register');

$router->group(['prefix' => 'account', 'middleware' => 'auth:api'], function () use ($router) {
    Route::post('changepassword', 'AccountController@changePassword');
    Route::get('', 'AccountController@info');
});

//Company controller
$router->group(['prefix' => 'company', 'middleware' => ['auth:api']], function () use ($router) {
    Route::post('create', ['uses' => 'CompanyController@create', 'middleware' => ['role:Admin']]);
    Route::post('update', ['uses' => 'CompanyController@update', 'middleware' => ['CompanyRole:Admin,false']]);
    Route::post('paymentservice/add', ['uses' => 'CompanyController@addPaymentService', 'middleware' => ['CompanyRole:Admin,false']]);
    Route::delete('paymentservice/remove', ['uses' => 'CompanyController@removePaymentService', 'middleware' => ['CompanyRole:Admin,false']]);
    Route::post('members/add', ['uses' => 'CompanyController@addMember', 'middleware' => ['CompanyRole:Manager,false']]);
    Route::delete('members/remove', ['uses' => 'CompanyController@removeMember', 'middleware' => ['CompanyRole:Manager,false']]);
    Route::patch('members/update', ['uses' => 'CompanyController@updateMemeber', 'middleware' => ['CompanyRole:Manager,false']]);
});
Route::get('company/{id}', ['uses' => 'CompanyController@get']);

//Event controller
$router->group(['prefix' => 'event', 'middleware' => ['auth:api']], function () use ($router) {
    Route::post('create', ['uses' => 'EventController@create', 'middleware' => ['CompanyRole:Manager,false']]);
    Route::delete('delete', ['uses' => 'EventController@delete', 'middleware' => ['CompanyRole:Manager,true']]);
    Route::post('update', ['uses' => 'EventController@update', 'middleware' => ['CompanyRole:Manager,true']]);
});
Route::get('event/{id}', ['uses' => 'EventController@get']);
Route::get('events', ['uses' => 'EventController@getall']);

//Ticket controller
$router->group(['prefix' => 'ticket', 'middleware' => ['auth:api']], function () use ($router) {
    Route::post('verify', ['uses' => 'TicketController@verify', 'middleware' => ['CompanyRole:Staff,true']]);
});

//Ticket Type controller
$router->group(['prefix' => 'ticket/type', 'middleware' => ['auth:api']], function () use ($router) {
    Route::post('create', ['uses' => 'TicketTypeController@create', 'middleware' => ['CompanyRole:Manager,true']]);
    Route::delete('delete', ['uses' => 'TicketTypeController@delete', 'middleware' => ['CompanyRole:Manager,true']]);
    Route::patch('update', ['uses' => 'TicketTypeController@update', 'middleware' => ['CompanyRole:Manager,true']]);
});
Route::get('ticket/types/{eventId}', ['uses' => 'TicketTypeController@getall']);

//Ticket status controller
Route::get('ticket/statuses/', ['uses' => 'TicketStatusController@getall']);
