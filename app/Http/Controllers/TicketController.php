<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{

    /**
     * verify a ticket
     *
     * This endpoint is used to verify if a ticket is valid
     * Role required:
     * Admin
     * Event-Manager
     * Event-Staff
     *
     * @group Ticket
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam event_id string required the event id
     * @bodyParam ticket_id string the ticket id
     * @bodyParam ticket_code string the manual ticket code.
     *
     * @response {
     *  "status": "success",
     *  "message": "The ticket has been scanend successfully and is valid!",
     *  "ticket": {
     *      "type_id": 3,
     *      "type": "Normal Ticket"
     *  }
     * */
    public function verify(Request $request){

        $rules = [
            "event_id" => "required|exists:events,id",
            "ticket_id" => "max:36|exists:tickets,id",
            "ticket_code" => "max:20|exists:tickets,random_number"
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        if($request->post("ticket_id") == null && $request->post("ticket_code") == null){
            return response()->json([
                "status" => "error",
                "message" => "Please use ticket_id or ticket_code!"
            ], 400);
        }

        $eventId = $request->post("event_id");

        $selectValue = ($request->post("ticket_id") != null ? ["id", $request->post("ticket_id")] : ["random_number", $request->post("ticket_code")]);

        $ticket = Ticket::with("ticketType")->whereHas('ticketType',function($type)use($eventId){
            $type->where("event_id",$eventId);
        })->where($selectValue[0], $selectValue[1]);

        if($ticket->exists()){

            $ticket = $ticket->first();

            return response()->json([
                "status" => "success",
                "message" => "The ticket has been scanend successfully and is valid!",
                "ticket" => [
                    "type_id" => $ticket->ticketType->id,
                    "type" => $ticket->ticketType->name
                ]
            ]);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "The ticket doesnt exists or is not valid!"
            ], 404);
        }

    }

}
