<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Tymon\JWTAuth\Facades\JWTAuth;

class AccountController extends Controller
{

    /**
     * Login
     *
     * This endpoint is used to login and generate a token
     *
     * @group Account
     *
     * @bodyParam email string required the email
     * @bodyParam password string required the password
     *
     * @response {
     *  "status": "success",
     *  "message": "Your were logged in successfully.",
     *  "token": {
     *      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwNzM0MTg3LCJleHAiOjE2MjA3NzczODcsIm5iZiI6MTYyMDczNDE4NywianRpIjoicGU4UlZadllkZmlVdkZiSyIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.concrQNIklIQoO20R1DJNYIt_N0wvP64LHnXYZKJ9KI",
     *      "type": "bearer",
     *      "expire": 43200
     *  }
     *}
     * */
    public function login(Request $request){

        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ]);
        }

        if(!$token = auth()->attempt(["email" => $request->post("email"), "password" => $request->post("password")])){
            return response()->json([
                "status" => "error",
                "message" => "Incorrect login information!"
            ], 401);
        }


        $auth = auth()->factory();

        //get user from session = $account = Account::where("id", JWTAuth::user()->id)->first();

        return response()->json([
            "status" => "success",
            "message" => "Your were logged in successfully.",
            "token" => [
                "token" => $token,
                "type" => "bearer",
                "expire" => ($auth->getTTL() * 60)
            ]
        ]);
    }

    /**
     * Register
     *
     * This endpoint is used to create an account
     *
     * @group Account
     *
     * @bodyParam firstname string required the firstname
     * @bodyParam lastname string required the lastname
     * @bodyParam email string required the email
     * @bodyParam password string required the password
     *
     * @response {
     *  "status": "success",
     *  "message": "Your account was created successfully"
     * }
     * */
    public function register(Request $request){

        $rules = [
            'firstname' => 'required|max:20',
            'lastname' => 'required|max:30',
            'email' => 'required|email|unique:accounts',
            'password' => 'required'

        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }


        $account = new Account();
        $account->id = Uuid::uuid4()->toString();
        $account->firstname = $request->post("firstname");
        $account->lastname = $request->post("lastname");
        $account->email = $request->post("email");
        $account->password = password_hash($request->post("password"), PASSWORD_BCRYPT);
        $account->save();

        return response()->json([
            "status" => "success",
            "message" => "Your account was created successfully"
        ]);

    }

    /**
     * Change password
     *
     * This endpoint is used to update the account password
     * (Will log you out)
     *
     * @group Account
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam password string required the old password
     * @bodyParam newPassword string required the new password
     * @bodyParam confirmNewPassword string required the confirmation of the new password
     *
     * @response {
     *  "status": "success",
     *  "message": "Your password has been updated successfully"
     * }
     * */
    public function changePassword(Request $request){
        $rules = [
            'password' => 'required',
            'newPassword' => 'required',
            'confirmNewPassword' => 'required',
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $newPassword = $request->post("newPassword");

        if($newPassword !== $request->post("confirmNewPassword")){
            return response()->json([
                "status" => "error",
                "message" => "Password and ConfirmPassword has to match each other."
            ], 400);
        }

        $password = $request->post("password");

        $account = Account::where("id", auth()->user()->getAuthIdentifier());

        if(!password_verify($password, $account->first()->password)){
            return response()->json([
                "status" => "error",
                "message" => "Incorrect password."
            ], 400);
        }

        $account->update(["password" => password_hash($newPassword, PASSWORD_BCRYPT)]);
        auth()->logout();

        return response()->json([
            "status" => "success",
            "message" => "Your password has been updated successfully"
        ]);

    }

    /**
     * Get basic information
     *
     * This endpoint is used to get all the account information
     *
     * @group Account
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @response {
     *  "status": "success",
     *  "message": "Got the account information successfully",
     *  "user": {
     *      "id": "600b01e1-34c7-4288-b4d7-52205fd30f7a",
     *      "firstname": "Admin",
     *      "lastname": "Admin",
     *      "groups": [
     *          {
     *              "id": 3,
     *              "name": "Admin",
     *              "pivot": {
     *                  "model_id": "600b01e1-34c7-4288-b4d7-52205fd30f7a",
     *                  "role_id": 3,
     *                  "model_type": "App\\Models\\Account"
     *              }
     *          }
     *      ],
     *    "companies": [
     *      {
     *          "id": "69051681-fde4-4158-bde5-8c0c4db3dd98",
     *          "name": "BookME",
     *          "role_id": 1,
     *          "laravel_through_key": "bd56e9f8-5275-41bb-be87-f581d2733cfb"
     *      }
     * ],
     *    "companyEvents": [
     *    {
     *      "id": "ab12a170-3c50-40b3-94c7-a03475741e24",
     *      "title": "Test",
     *      "company_id": "51bcf94c-6777-4038-9ad5-87b88b8373b9",
     *      "laravel_through_key": "e580d248-3dd9-460c-bb1e-9ad3a40d7363"
     *    }
     *  ]
     *}
     * */
    public function info(){

        $account = Account::Where("id", auth()->user()->getAuthIdentifier())->with("roles:id,name")->with("companies")->with("companyEvents")->first();

        return response()->json([
            "status" => "success",
            "message" => "Got the account information successfully",
            "user" => [
                "id" => $account->id,
                "firstname" => $account->firstName,
                "lastname" => $account->lastName,
                "groups" => $account->roles,
                "companies" => $account->companies,
                "companyEvents" => $account->companyEvents
            ]
        ]);
    }
}
