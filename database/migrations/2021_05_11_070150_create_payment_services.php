<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_services', function (Blueprint $table) {
            $table->integer("id")->autoIncrement();
            $table->integer("payment_type_id")->nullable(false);
            $table->char("company_id", 36)->nullable(false);
            $table->string("client_id")->nullable(false);
            $table->string("secret_id")->nullable(false);

            $table->foreign("payment_type_id")->references("id")->on("payment_types");
            $table->foreign("company_id")->references("id")->on("companies");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_services');
    }
}
