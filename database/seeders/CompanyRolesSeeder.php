<?php

namespace Database\Seeders;

use App\Models\CompanyRole;
use App\Models\TicketStatus;
use Illuminate\Database\Seeder;

class CompanyRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $companyRoles = array(
            array("name" => "Admin"),
            array("Manager"),
            array("Staff"));

        CompanyRole::insert($companyRoles);
    }
}
