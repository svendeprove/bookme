<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Address;
use App\Models\Event;
use App\Models\EventImage;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class EventController extends Controller
{

    /**
     * Create an event
     *
     * This endpoint is used to create an event
     * Role required:
     * Admin
     * Company Admin
     * Company Manager
     *
     * @group Event
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam company_id string required the id of the company
     * @bodyParam title string required the title of the event
     * @bodyParam country string required the country of the event
     * @bodyParam desc string required the description of the event
     * @bodyParam city string required the city of the event
     * @bodyParam address string required the address of the event
     * @bodyParam date integer required the date of the event
     * @bodyParam image file the event image
     * @bodyParam images file[] the event images
     *
     * @response {
     *  "status": "success",
     *  "message": "The event has been created successfully"
     * }
     * */
    public function create(Request $request){

        $rules = [
            "company_id" => "required|max:36|exists:companies,id",
            "title" => "required|max:45",
            "desc" => "required",
            "country" => "required|max:50",
            "city" => "required|max:100",
            "address" => "required|max:120",
            "date" => "required|integer",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }


        $address = Address::firstOrCreate(["country" => $request->post("country"), "city" => $request->post("city"), "address" => $request->post("address")]);

        $eventId = Uuid::uuid4();

        $event = new Event();
        $event->id = $eventId;
        $event->company_id = $request->post("company_id");
        $event->account_id = auth()->user()->getAuthIdentifier();
        $event->title = $request->post("title");
        $event->desc = $request->post("desc");
        $event->address_id = $address->id;
        $event->created = time();
        $event->date = $request->post("date");


        if(($file = $request->file("image")) != null){

            if(strtolower($file->getClientOriginalExtension()) == "png" || strtolower($file->getClientOriginalExtension()) == "jpg" || strtolower($file->getClientOriginalExtension()) == "jpeg") {

                if($file->getSize() < 1000000 && $file->isValid()){
                    $name = bin2hex(openssl_random_pseudo_bytes(10)). ".". $file->getClientOriginalExtension();
                    $path = "data/events/". $eventId. "/images";
                    $file->move($path, $name);

                    $event->image_path = $path. "/". $name;
                }
            }
        }
        $event->save();


        if(($files = $request->file("images")) != null){

            $imagesInsertData = array();
            foreach ($files as $extraFile){

                if(strtolower($extraFile->getClientOriginalExtension()) == "png" || strtolower($extraFile->getClientOriginalExtension()) == "jpg" || strtolower($extraFile->getClientOriginalExtension()) == "jpeg") {

                    if($extraFile->getSize() < 1000000 && $extraFile->isValid()){

                        $name = bin2hex(openssl_random_pseudo_bytes(10)). ".". $extraFile->getClientOriginalExtension();
                        $path = "data/events/". $eventId. "/images";
                        $extraFile->move($path, $name);

                        $imagesInsertData[] = array("event_id" => $eventId, "path" => $path. "/". $name);
                    }
                }

            }
            EventImage::insert($imagesInsertData);
        }

        return response()->json([
            "status" => "success",
            "message" => "The event has been created successfully"
        ]);
    }

    /**
     * Delete an event
     *
     * This endpoint is used to delete an event (cannot be deleted if any tickets is sold)
     * Role required:
     * Admin
     * Company Admin
     * Company Manager
     *
     * @group Event
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam event_id string required the uuid of the event
     *
     * @response {
     *  "status": "success",
     *  "message": "The event were successfully deleted!"
     * }
     * */
    public function delete(Request $request){

        $rules = [
            "event_id" => "required",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $getEvent = Event::Where("id", $request->post("event_id"))->with("tickets");


        if(!$getEvent->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No events with this id exists!"
            ], 404);
        }

        if(count($getEvent->first()->tickets) > 0){
            return response()->json([
                "status" => "error",
                "message" => "Events that already have tickets sold, cannot be deleted!"
            ], 400);
        }


        $getEvent->first()->delete();
        return response()->json([
            "status" => "success",
            "message" => "The event were successfully deleted!"
        ]);
    }

    /**
     * Get a specific event
     *
     * This endpoint is used to get the specific information about an event
     *
     * @group Event
     *
     * @queryParam id required the uuid of the event
     *
     * @response {
     *  "status": "success",
     *  "message": "Successfully got all the upcoming events",
     *  "event": {
     *      "id": "3a8ba813-c754-4e98-814c-f707f44ac919",
     *      "title": "test",
     *      "desc": "test",
     *      "image_path": "test image",
    *       "date": "320320320"
    *       "address": {
    *           "country": null,
    *           "city": "test",
    *           "address": "test"
    *       },
    *       "company": {
    *           "id": "69051681-fde4-4158-bde5-8c0c4db3dd98",
    *           "name": "BigBoyCompany"
    *       },
    *       "images": [],
    *       "ticket_types": [
    *       {
    *           "id": 3,
    *           "event_id": "3a8ba813-c754-4e98-814c-f707f44ac919",
    *           "name": "VIP",
    *           "desc": "Super VIP ticket",
    *           "price": 2500,
    *           "available": 10
    *       }
    *   ],
    *   "stats": {
    *       "total_sold": 2,
    *       "total_sold_amount": 4500,
    *       "total_refunded": 0,
    *       "total_refunded_amount": 0
    *    }
    * }
     * }
     * */
    public function get($id){

        $getEvent = Event::Where("id", $id)
            ->with("address")
            ->with("images")
            ->with("ticketTypes")
            ->with("tickets")
            ->with("company")
            ->select(["id", "title", "desc", "image_path", "address_id", "company_id", "date"]);

        if(!$getEvent->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No event with this id exists!"
            ], 404);
        }

        $getEvent = $getEvent->first();

        $tickets = $getEvent->tickets->toArray();

        $soldTickets = array_filter($tickets, function($obj) {return $obj["status_id"] == 1;}, ARRAY_FILTER_USE_BOTH);
        $soldTicketsAmount = array_sum(array_map(function($obj) { return $obj["sold_price"]; }, $soldTickets));

        $refundedTickets = array_filter($tickets, function($obj) { return $obj["status_id"] == 2; }, ARRAY_FILTER_USE_BOTH);
        $refundedTicketsAmount = array_sum(array_map(function($obj) { return $obj["sold_price"]; }, $refundedTickets));

        return response()->json([
            "status" => "success",
            "message" => "Successfully found the event",
            "event" => [
                "id" => $getEvent->id,
                "title" => $getEvent->title,
                "desc" => $getEvent->desc,
                "image_path" => $getEvent->image_path,
                "date" => $getEvent->date,
                "address" => [
                    "country" => $getEvent->address->country,
                    "city" => $getEvent->address->city,
                    "address" => $getEvent->address->address
                ],
                "company" => $getEvent->company,
                "images" => $getEvent->images,
                "ticket_types" => $getEvent->ticketTypes,
                "stats" => [
                    "total_sold" => count($soldTickets),
                    "total_sold_amount" => $soldTicketsAmount,
                    "total_refunded" => count($refundedTickets),
                    "total_refunded_amount" => $refundedTicketsAmount,

                ]
            ]
        ]);

    }

    /**
     * Get all the events
     *
     * This endpoint is used to get a list of all upcoming events
     *
     * @group Event
     *
     * @response {
     *  "status": "success",
     *  "message": "Successfully got all the upcoming events",
     *  "events": [
     *      {
     *          "id": "3a8ba813-c754-4e98-814c-f707f44ac919",
     *          "title": "test",
     *          "image_path": "data/events/f0e25e92-8b29-4e3e-83e7-692317ce5411/images/6550467992d1bd36e639.png",
     *          "date": 1626510686
     *      },
     *  ]
     * }
     * */
    public function getAll(){

        $events = Event::where("date", ">=", (time()-86400))->select(["id", "title", "image_path", "date"])->get();

        if(count($events) == 0){
            return response()->json([
                "status" => "error",
                "message" => "No upcoming events found!"
            ], 404);
        }

        return response()->json([
            "status" => "success",
            "message" => "Successfully got all the upcoming events",
            "events" => $events
        ]);



    }

    /**
     * Update an event
     *
     * This endpoint is used to update an event
     * Role required:
     * Admin
     * Company Admin
     * Company Manager
     *
     * @group Event
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam event_id string required the id of the event
     *
     * @bodyParam title string the title of the event
     * @bodyParam desc string the description of the event
     * @bodyParam country string the country of the event
     * @bodyParam city string the city of the event
     * @bodyParam address string the address of the event
     * @bodyParam date integer the date of the event
     * @bodyParam image file the event image
     * @bodyParam images file[] the event images
     *
     * @response {
     *  "status": "success",
     *  "message": "The event has been created successfully"
     * }
     * */
    public function update(Request $request){

        $rules = [
            "event_id" => "required",
            "title" => "max:45",
            "country" => "max:50",
            "city" => "max:100",
            "address" => "max:120",
            "date" => "integer"
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }


        $getEvent = Event::Where("id", $request->post("event_id"));


        if(!$getEvent->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No event with this id exists!"
            ], 404);
        }

        $eventId = $getEvent->first()->id;
        $updateValues = [];

        //Updating the normal fields
        $paramNames = array("title", "desc");
        foreach ($paramNames as $paramName){

            if(($param = $request->post($paramName)) != null){
                $updateValues[$paramName] = $param;
            }

        }

        //Updating the address
        if($request->post("city") != null && $request->post("address") != null){
            $address = Address::firstOrCreate(["city" => $request->post("city"), "address" => $request->post("address"), "country" => $request->post("country")]);
            $updateValues["address_id"] = $address->id;
        }


        //Updating the images

        //The single image
        if(($file = $request->file("image")) != null){

            if(strtolower($file->getClientOriginalExtension()) == "png" || strtolower($file->getClientOriginalExtension()) == "jpg" || strtolower($file->getClientOriginalExtension()) == "jpeg") {

                if($file->getSize() < 1000000 && $file->isValid()){
                    $name = bin2hex(openssl_random_pseudo_bytes(10)). ".". $file->getClientOriginalExtension();
                    $path = "data/events/". $eventId. "/images";
                    $file->move($path, $name);

                    $updateValues["image_path"] = $path. "/". $name;
                }
            }
        }

        if(($files = $request->file("images")) != null){

            $imagesInsertData = array();
            foreach ($files as $extraFile){

                if(strtolower($extraFile->getClientOriginalExtension()) == "png" || strtolower($extraFile->getClientOriginalExtension()) == "jpg" || strtolower($extraFile->getClientOriginalExtension()) == "jpeg") {

                    if($extraFile->getSize() < 1000000 && $extraFile->isValid()){

                        $name = bin2hex(openssl_random_pseudo_bytes(10)). ".". $extraFile->getClientOriginalExtension();
                        $path = "data/events/". $eventId. "/images";
                        $extraFile->move($path, $name);

                        $imagesInsertData[] = array("event_id" => $eventId, "path" => $path. "/". $name);
                    }
                }

            }
            EventImage::insert($imagesInsertData);
        }


        //When all the data has been fetched etc it will be update in this qurry here
        if(count($updateValues) > 0)
            $getEvent->update($updateValues);


        return response()->json([
            "status" => "success",
            "message" => "Event has been updated successfully"
        ]);

    }
}
