<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{
    public $timestamps = false;

    public function event()
    {
        return $this->belongsTo(Event::class, "event_id","id");
    }

    public function tickets(){
        return $this->hasMany(Ticket::class, "ticket_type_id", "id");
    }
}

