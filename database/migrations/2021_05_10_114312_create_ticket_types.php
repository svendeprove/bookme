<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_types', function (Blueprint $table) {
            $table->integer("id")->autoIncrement();
            $table->char("event_id", 36)->nullable(false);
            $table->string("name", 30)->nullable(false);
            $table->string("desc", 120)->nullable(false);
            $table->double("price")->nullable(false);
            $table->integer("available")->nullable(false);

            $table->foreign("event_id")->references("id")->on("events");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_types');
    }
}
