<?php


namespace App\Http\Middleware;

use App\Events\Event;
use App\Models\Company;
use App\Models\Ticket;
use Closure;
use Illuminate\Support\Facades\Validator;

class CompanyRole
{
    public $attributes;

    public function handle($request, Closure $next, $roleName, $eventCheck)
    {

        $rules = [
            "company_id" => "max:36|exists:companies,id",
            "event_id" => "max:36|exists:events,id"
        ];

        $validator = Validator::make(request()->post(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        if($request->post("event_id") != null && $eventCheck == "true"){
            $companyId = \App\Models\Event::where("id", $request->post("event_id"))->first()->company_id;
        }else if($request->post("company_id") != null){
            $companyId = $request->post("company_id");
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Missing company_id or event_id!"
            ], 400);
        }

        $roleId = \App\Models\CompanyRole::where("name", $roleName);

        if(!$roleId->exists()){
            return response()->json([
                "status" => "error",
                "message" => "Unknown role"
            ], 404);
        }

        $roleId = $roleId->first()->id;
        $userId = auth()->user()->getAuthIdentifier();


        $getCompany = Company::with("members")->whereHas('members',function($type)use($roleId, $userId){
            $type->where([["account_id", $userId], ["role_id", "<=", $roleId]]);

        })->where("id", $companyId);

        if(!auth()->user()->hasRole("Admin") && !$getCompany->exists()){
            return response()->json([
                "status" => "error",
                "message" => "You are not permitted to do this, due to lack of permissions"
            ], 401);
        }


        $members = $getCompany->first()->members->toArray();
        $getCurrnetUserAsMember = array_filter($members, function($obj) use ($userId) { return $obj["id"] == $userId; }, ARRAY_FILTER_USE_BOTH);

        $request->attributes->add(['currentUserRole' => array_values($getCurrnetUserAsMember)[0]["role_id"]]);

        return $next($request);
    }
}
