<?php

namespace App\Http\Controllers;

use App\Models\TicketType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketTypeController extends Controller
{
    /**
     * Create a Ticket Type
     *
     * This endpoint is used to create a ticket type
     * Role required:
     * Admin
     * Event-Manager
     *
     * @group TicketType
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam event_id string required the id of the event
     * @bodyParam name string required the name of the ticket type
     * @bodyParam desc string required a short description of the ticket type
     * @bodyParam price integer required the price of the ticket type
     * @bodyParam available integer how many of the ticket types that are available to be sold
     *
     * @response {
     *  "status": "success",
     *  "message": "The ticket type has been created successfully"
     * }
     * */
    public function create(Request $request){

        $rules = [
            "event_id" => "required|max:36|exists:events,id",
            "name" => "required|max:30",
            "desc" => "required|max:120",
            "price" => "required",
            "available" => "required",
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $ticketType = new TicketType();
        $ticketType->event_id = $request->post("event_id");
        $ticketType->name = $request->post("name");
        $ticketType->desc = $request->post("desc");
        $ticketType->price = $request->post("price");
        $ticketType->available = $request->post("available");
        $ticketType->save();

        return response()->json([
            "status" => "success",
            "message" => "The ticket type has been created successfully"
        ]);

    }

    /**
     * Delete a Ticket Type
     *
     * This endpoint is used to delete a ticket type (can only be deleted if no tickets of this type, has been sold)
     * Role required:
     * Admin
     * Event-Manager
     *
     * @group TicketType
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam id integer required the id of the ticket type
     * @bodyParam event_id string required the id of the event
     *
     * @response {
     *  "status": "success",
     *  "message": "The ticket type has been deleted successfully"
     * }
     * */
    public function delete(Request $request){

        $rules = [
            "id" => "required|integer|exists:ticket_types,id",
            "event_id" => "required|exists:events,id"
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $getTicketType = TicketType::Where([["id", $request->post("id")], ["event_id", $request->post("event_id")]])->with("tickets");

        if(!$getTicketType->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No ticket type with this id, was found for this event"
            ], 404);
        }

        $getTicketType = $getTicketType->first();

        if(count($getTicketType->tickets) > 0){
            return response()->json([
                "status" => "error",
                "message" => "A ticket of this type has already been sold, so it cant be deleted!"
            ], 400);
        }

        $getTicketType->delete();

        return response()->json([
            "status" => "success",
            "message" => "The ticket type has been deleted successfully"
        ]);

    }

    /**
     * Get all Ticket Types
     *
     * This endpoint is used to get all the ticket types
     *
     * @group TicketType
     *
     * @queryParam event_id string required the id of the event
     *
     * @response {
     *  "status": "success",
     *  "message": "The ticket types has been fetched successfully",
     *  "ticket_types": [
     *  {
     *      "id": 3,
     *      "event_id": "3a8ba813-c754-4e98-814c-f707f44ac919",
     *      "name": "VIP",
     *      "desc": "Super VIP ticket",
     *      "price": 2500,
     *      "available": 10
     *  }
     *]
     * }
     * */
    public function getAll($eventId){

        $ticketTypes = TicketType::where("event_id", $eventId);

        if(!$ticketTypes->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No event or ticket types were found!"
            ], 404);
        }

        return response()->json([
            "status" => "success",
            "message" => "The ticket types has been fetched successfully",
            "ticket_types" => $ticketTypes->get()
        ]);
    }

    /**
     * Update a Ticket Type
     *
     * This endpoint is used to update an event
     * Role required:
     * Admin
     * Event-Manager
     *
     * @group TicketType
     *
     * @authenticated
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE
     *
     * @bodyParam id string required the id of the ticket type
     * @bodyParam event_id integer required the id of the event
     *
     * @bodyParam title string the title of the event
     * @bodyParam desc string the description of the event
     * @bodyParam country string the country of the event
     * @bodyParam city string the city of the event
     * @bodyParam address string the address of the event
     * @bodyParam date integer the date of the event
     * @bodyParam image file the event image
     * @bodyParam images file[] the event images
     *
     * @response {
     *  "status": "success",
     *  "message": "The ticket type has been updated successfully"
     * }
     * */
    public function update(Request $request){

        $rules = [
            "id" => "required|integer|exists:ticket_types,id",
            "event_id" => "required|exists:events,id",
            "name" => "max:30",
            "desc" => "max:130",
            "price" => "integer",
            "available" => "integer"
        ];

        $validator = Validator::make($request->post(), $rules);

        if($validator->fails()){

            return response()->json([
                "status" => "error",
                "message" => $validator->errors()->first()
            ], 400);
        }

        $updateValues = [];

        $getTicketType = TicketType::Where([["id", $request->post("id")], ["event_id", $request->post("event_id")]]);

        if(!$getTicketType->exists()){
            return response()->json([
                "status" => "error",
                "message" => "No ticket type with this id, was found for this event"
            ], 404);
        }

        //Updating values
        $paramNames = array("name", "desc", "price", "available");
        foreach ($paramNames as $paramName){

            if(($param = $request->post($paramName)) != null){
                $updateValues[$paramName] = $param;
            }
        }

        if(count($updateValues) > 0)
            $getTicketType->update($updateValues);

        return response()->json([
            "status" => "success",
            "message" => "The ticket type has been updated successfully"
        ]);

    }
}
