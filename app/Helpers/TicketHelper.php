<?php


namespace App\Helpers;


use App\Models\Ticket;
use Ramsey\Uuid\Uuid;

class TicketHelper
{

    public function create($ticketType, $price, $orderId, $paymentType, $email){

        $ticket = new Ticket();

        $ticket->id = Uuid::uuid4();
        $ticket->random_number = bin2hex(openssl_random_pseudo_bytes(10));
        $ticket->ticket_type_id = $ticketType;

        $ticket->email = $email;
        $ticket->sold_price = $price;
        $ticket->date = time();
        $ticket->order_id = $orderId;
        $ticket->payment_type = $paymentType;
        $ticket->status_id = 3;
        $ticket->save();
    }

}
