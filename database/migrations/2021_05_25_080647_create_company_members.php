<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_members', function (Blueprint $table) {
            $table->integer("id")->autoIncrement();
            $table->char("company_id", 36)->nullable(false);
            $table->char("account_id", 36)->nullable(false);
            $table->integer("role_id")->nullable(false);
            
            $table->foreign("company_id")->references("id")->on("companies");
            $table->foreign("account_id")->references("id")->on("accounts");
            $table->foreign("role_id")->references("id")->on("company_roles");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_members');
    }
}
