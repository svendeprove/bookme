<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Event extends Model
{
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($event) {

            $event->ticketTypes()->delete();

            $path = base_path(). "/public/data/events/". $event->id;
            File::deleteDirectory($path);

            $event->images()->delete();

        });
    }

    public function company(){
        return $this->hasOne(Company::class, "id", "company_id");
    }

    public function address(){
        return $this->hasOne(Address::class, "id", "address_id");
    }

    public function images(){
        return $this->hasMany(EventImage::class, "event_id", "id");
    }

    public function ticketTypes()
    {
        return $this->hasMany(TicketType::class, "event_id", "id");
    }

    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, TicketType::class, "event_id", "ticket_type_id", "id", "id");
    }

}
