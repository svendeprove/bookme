<?php


namespace App\Helpers;


use App\Models\Ticket;
use Illuminate\Support\Facades\App;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PdfHelper
{

    public function generate(Ticket $ticket){
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('
        <style>
           @page { margin: 0px; }
            body { padding: 0px 0px 0px 0px; margin: 0 auto;}

            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                padding-right: 30px;
                padding-left: 30px;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }

        </style>

        <body>
            <div style="padding: 0px 0px 0px 0px; width: 24em; height: 8em; margin-right: auto; margin-left: auto;">
                <img style="margin-left: 7em; display: block; width: 10em; height: 10em;" src="../resources/ticket/logo.png">
                <p style="text-align: center; font-size: 32px; font-weight: bold; margin-top: -20px;">BookMe</p>
            </div>

            <div style="padding: 0px 0px 0px 0px; width: 24em; height: 5em; margin-right: auto; margin-left: auto;">
                <p style="text-align: center; font-size: 23px; font-weight: bold;">'.($ticket->account != null ? "Kære ". $ticket->account->firstName. " ". $ticket->account->lastName : "Kære Kunde").'</p>
                <p style="text-align: center; margin-top: -20px">Her er din kvertting og billet</p>
            </div>

            <p style="text-align: center; font-weight: bold; font-size: 22px;">Kvittering:</p>
            <table>
                <tr>
                    <th>Billet navn:</th>
                    <th>Antal</th>
                    <th>Pris</th>
                </tr>
                <tr>
                    <td>'.$ticket->ticketType->name.'</td>
                    <td>1</td>
                    <td>'.$ticket->sold_price.'</td>
                </tr>
            </table>

            <p style="text-align: center; font-weight: bold; font-size: 22px;">Billet:</p>
            <div style="padding: 0px 0px 0px 0px; margin-top: 1em; width: 24em; height: 15em; margin-right: auto; margin-left: auto;">
            <div style="padding: 0px 0px 0px 0px; margin-top: 1em; width: 24em; height: 15em; margin-right: auto; margin-left: auto;">
                <img style="display: block; margin: 0 auto; margin-left: 5em;" src="data:image/png;base64,'. base64_encode(QrCode::size(200)->generate($ticket->id)) .'">
                <p style="margin-top: 11em; font-size: 18px; text-align: center;">Manual code:</p>
                <p style="margin-top: -20px; font-size: 18px; text-align: center;">'.$ticket->random_number.'</p>
            </div>

        </body>
    ');
        $pdf->save(base_path(). "/resources/tickets/pdfs/". $ticket->id. ".pdf");
    }

    public function get($id){
        return base_path(). "/resources/tickets/pdfs/". $id. ".pdf";
    }

}
