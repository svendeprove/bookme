<?php

namespace App\Http\Controllers;

use App\Models\TicketStatus;
use App\Models\TicketType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketStatusController extends Controller
{

    /**
     * Get all Ticket statuses
     *
     * This endpoint is used to get all the ticket statuses
     *
     * @group TicketStatus
     *
     * @queryParam event_id string required the id of the event
     *
     * @response {
     *  "status": "success",
     *  "message": "The ticket statuses has been fetched successfully",
     *  "ticket_statuses": [
     *   {
     *      "id": 1,
     *      "name": "SOLD"
     *  },
     * ]
     * }
     * */
    public function getAll(){

        $statuses = TicketStatus::all();

        return response()->json([
            "status" => "success",
            "message" => "The ticket statuses has been fetched successfully",
            "ticket_statuses" => $statuses
        ]);

    }
}
