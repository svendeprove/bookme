<?php

namespace Database\Seeders;

use App\Models\EventImage;
use App\Models\TicketStatus;
use Illuminate\Database\Seeder;

class TicketStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = array(
            array("name" => "SOLD"),
            array("REFUNDED"),
            array("PROCESSING"));

        TicketStatus::insert($statuses);
    }
}
