# Event


## Create an event

<small class="badge badge-darkred">requires authentication</small>

This endpoint is used to create an event
Role required:
Admin
Event-Manager

> Example request:

```bash
curl -X POST \
    "http://localhost/event/create" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE" \
    -F "title=aliquid" \
    -F "desc=ad" \
    -F "city=sed" \
    -F "address=reiciendis" \
    -F "date=19" \
    -F "image=@C:\Users\hund35\AppData\Local\Temp\php66.tmp"     -F "images[]=@C:\Users\hund35\AppData\Local\Temp\php67.tmp" 
```

```javascript
const url = new URL(
    "http://localhost/event/create"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE",
};

const body = new FormData();
body.append('title', 'aliquid');
body.append('desc', 'ad');
body.append('city', 'sed');
body.append('address', 'reiciendis');
body.append('date', '19');
body.append('image', document.querySelector('input[name="image"]').files[0]);
body.append('images[]', document.querySelector('input[name="images[]"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/event/create',
    [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE',
        ],
        'multipart' => [
            [
                'name' => 'title',
                'contents' => 'aliquid'
            ],
            [
                'name' => 'desc',
                'contents' => 'ad'
            ],
            [
                'name' => 'city',
                'contents' => 'sed'
            ],
            [
                'name' => 'address',
                'contents' => 'reiciendis'
            ],
            [
                'name' => 'date',
                'contents' => '19'
            ],
            [
                'name' => 'image',
                'contents' => fopen('C:\Users\hund35\AppData\Local\Temp\php66.tmp', 'r')
            ],
            [
                'name' => 'images[]',
                'contents' => fopen('C:\Users\hund35\AppData\Local\Temp\php67.tmp', 'r')
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/event/create'
files = {
  'image': open('C:\Users\hund35\AppData\Local\Temp\php66.tmp', 'rb')  'images[]': open('C:\Users\hund35\AppData\Local\Temp\php67.tmp', 'rb')
}
payload = {
    "title": "aliquid",
    "desc": "ad",
    "city": "sed",
    "address": "reiciendis",
    "date": 19
}
headers = {
  'Content-Type': 'multipart/form-data',
  'Accept': 'application/json',
  'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE'
}

response = requests.request('POST', url, headers=headers, files=files, data=payload)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The event has been created successfully"
}
```
<div id="execution-results-POSTevent-create" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTevent-create"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTevent-create"></code></pre>
</div>
<div id="execution-error-POSTevent-create" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTevent-create"></code></pre>
</div>
<form id="form-POSTevent-create" data-method="POST" data-path="event/create" data-authed="1" data-hasfiles="2" data-headers='{"Content-Type":"multipart\/form-data","Accept":"application\/json","Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE"}' onsubmit="event.preventDefault(); executeTryOut('POSTevent-create', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTevent-create" onclick="tryItOut('POSTevent-create');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTevent-create" onclick="cancelTryOut('POSTevent-create');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTevent-create" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>event/create</code></b>
</p>
<p>
<label id="auth-POSTevent-create" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTevent-create" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>title</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="title" data-endpoint="POSTevent-create" data-component="body" required  hidden>
<br>
the title of the event
</p>
<p>
<b><code>desc</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="desc" data-endpoint="POSTevent-create" data-component="body" required  hidden>
<br>
the description of the event
</p>
<p>
<b><code>city</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="city" data-endpoint="POSTevent-create" data-component="body" required  hidden>
<br>
the city of the event
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="address" data-endpoint="POSTevent-create" data-component="body" required  hidden>
<br>
the address of the event
</p>
<p>
<b><code>date</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="date" data-endpoint="POSTevent-create" data-component="body" required  hidden>
<br>
the date of the event
</p>
<p>
<b><code>image</code></b>&nbsp;&nbsp;<small>file</small>     <i>optional</i> &nbsp;
<input type="file" name="image" data-endpoint="POSTevent-create" data-component="body"  hidden>
<br>
the event image
</p>
<p>
<b><code>images</code></b>&nbsp;&nbsp;<small>file[]</small>     <i>optional</i> &nbsp;
<input type="file" name="images.0" data-endpoint="POSTevent-create" data-component="body"  hidden>
<input type="file" name="images.1" data-endpoint="POSTevent-create" data-component="body" hidden>
<br>
the event images
</p>

</form>


## Delete an event

<small class="badge badge-darkred">requires authentication</small>

This endpoint is used to delete an event (cannot be deleted if any tickets is sold)
Role required:
Admin
Event-Manager

> Example request:

```bash
curl -X DELETE \
    "http://localhost/event/delete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE" \
    -d '{"id":"accusantium"}'

```

```javascript
const url = new URL(
    "http://localhost/event/delete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE",
};

let body = {
    "id": "accusantium"
}

fetch(url, {
    method: "DELETE",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/event/delete',
    [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE',
        ],
        'json' => [
            'id' => 'accusantium',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/event/delete'
payload = {
    "id": "accusantium"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE'
}

response = requests.request('DELETE', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The event were successfully deleted!"
}
```
<div id="execution-results-DELETEevent-delete" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEevent-delete"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEevent-delete"></code></pre>
</div>
<div id="execution-error-DELETEevent-delete" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEevent-delete"></code></pre>
</div>
<form id="form-DELETEevent-delete" data-method="DELETE" data-path="event/delete" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json","Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE"}' onsubmit="event.preventDefault(); executeTryOut('DELETEevent-delete', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEevent-delete" onclick="tryItOut('DELETEevent-delete');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEevent-delete" onclick="cancelTryOut('DELETEevent-delete');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEevent-delete" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>event/delete</code></b>
</p>
<p>
<label id="auth-DELETEevent-delete" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEevent-delete" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="DELETEevent-delete" data-component="body" required  hidden>
<br>
the uuid of the event
</p>

</form>


## Update an event

<small class="badge badge-darkred">requires authentication</small>

This endpoint is used to update an event
Role required:
Admin
Event-Manager

> Example request:

```bash
curl -X POST \
    "http://localhost/event/update" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE" \
    -F "id=delectus" \
    -F "title=saepe" \
    -F "desc=dolor" \
    -F "country=commodi" \
    -F "city=quibusdam" \
    -F "address=dignissimos" \
    -F "date=20" \
    -F "image=@C:\Users\hund35\AppData\Local\Temp\php69.tmp"     -F "images[]=@C:\Users\hund35\AppData\Local\Temp\php6A.tmp" 
```

```javascript
const url = new URL(
    "http://localhost/event/update"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE",
};

const body = new FormData();
body.append('id', 'delectus');
body.append('title', 'saepe');
body.append('desc', 'dolor');
body.append('country', 'commodi');
body.append('city', 'quibusdam');
body.append('address', 'dignissimos');
body.append('date', '20');
body.append('image', document.querySelector('input[name="image"]').files[0]);
body.append('images[]', document.querySelector('input[name="images[]"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/event/update',
    [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE',
        ],
        'multipart' => [
            [
                'name' => 'id',
                'contents' => 'delectus'
            ],
            [
                'name' => 'title',
                'contents' => 'saepe'
            ],
            [
                'name' => 'desc',
                'contents' => 'dolor'
            ],
            [
                'name' => 'country',
                'contents' => 'commodi'
            ],
            [
                'name' => 'city',
                'contents' => 'quibusdam'
            ],
            [
                'name' => 'address',
                'contents' => 'dignissimos'
            ],
            [
                'name' => 'date',
                'contents' => '20'
            ],
            [
                'name' => 'image',
                'contents' => fopen('C:\Users\hund35\AppData\Local\Temp\php69.tmp', 'r')
            ],
            [
                'name' => 'images[]',
                'contents' => fopen('C:\Users\hund35\AppData\Local\Temp\php6A.tmp', 'r')
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/event/update'
files = {
  'image': open('C:\Users\hund35\AppData\Local\Temp\php69.tmp', 'rb')  'images[]': open('C:\Users\hund35\AppData\Local\Temp\php6A.tmp', 'rb')
}
payload = {
    "id": "delectus",
    "title": "saepe",
    "desc": "dolor",
    "country": "commodi",
    "city": "quibusdam",
    "address": "dignissimos",
    "date": 20
}
headers = {
  'Content-Type': 'multipart/form-data',
  'Accept': 'application/json',
  'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE'
}

response = requests.request('POST', url, headers=headers, files=files, data=payload)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The event has been created successfully"
}
```
<div id="execution-results-POSTevent-update" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTevent-update"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTevent-update"></code></pre>
</div>
<div id="execution-error-POSTevent-update" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTevent-update"></code></pre>
</div>
<form id="form-POSTevent-update" data-method="POST" data-path="event/update" data-authed="1" data-hasfiles="2" data-headers='{"Content-Type":"multipart\/form-data","Accept":"application\/json","Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE"}' onsubmit="event.preventDefault(); executeTryOut('POSTevent-update', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTevent-update" onclick="tryItOut('POSTevent-update');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTevent-update" onclick="cancelTryOut('POSTevent-update');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTevent-update" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>event/update</code></b>
</p>
<p>
<label id="auth-POSTevent-update" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTevent-update" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="POSTevent-update" data-component="body" required  hidden>
<br>
the id of the event
</p>
<p>
<b><code>title</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="title" data-endpoint="POSTevent-update" data-component="body"  hidden>
<br>
the title of the event
</p>
<p>
<b><code>desc</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="desc" data-endpoint="POSTevent-update" data-component="body"  hidden>
<br>
the description of the event
</p>
<p>
<b><code>country</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="country" data-endpoint="POSTevent-update" data-component="body"  hidden>
<br>
the country of the event
</p>
<p>
<b><code>city</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="city" data-endpoint="POSTevent-update" data-component="body"  hidden>
<br>
the city of the event
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="address" data-endpoint="POSTevent-update" data-component="body"  hidden>
<br>
the address of the event
</p>
<p>
<b><code>date</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="date" data-endpoint="POSTevent-update" data-component="body"  hidden>
<br>
the date of the event
</p>
<p>
<b><code>image</code></b>&nbsp;&nbsp;<small>file</small>     <i>optional</i> &nbsp;
<input type="file" name="image" data-endpoint="POSTevent-update" data-component="body"  hidden>
<br>
the event image
</p>
<p>
<b><code>images</code></b>&nbsp;&nbsp;<small>file[]</small>     <i>optional</i> &nbsp;
<input type="file" name="images.0" data-endpoint="POSTevent-update" data-component="body"  hidden>
<input type="file" name="images.1" data-endpoint="POSTevent-update" data-component="body" hidden>
<br>
the event images
</p>

</form>


## Get all the events


This endpoint is used to get the spefic information about the event

> Example request:

```bash
curl -X GET \
    -G "http://localhost/event/vero?id=asperiores" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/event/vero"
);

let params = {
    "id": "asperiores",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/event/vero',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'query' => [
            'id'=> 'asperiores',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/event/vero'
params = {
  'id': 'asperiores',
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

response = requests.request('GET', url, headers=headers, params=params)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "Successfully got all the upcoming events",
    "event": {
        "id": "3a8ba813-c754-4e98-814c-f707f44ac919",
        "title": "test",
        "desc": "test",
        "      image_path": "test image",
        "address": {
            "country": null,
            "city": "test",
            "address": "test"
        },
        "images": [],
        "ticket_types": [
            {
                "id": 3,
                "event_id": "3a8ba813-c754-4e98-814c-f707f44ac919",
                "name": "VIP",
                "desc": "Super VIP ticket",
                "price": 2500,
                "available": 10
            }
        ],
        "stats": {
            "total_sold": 2,
            "total_sold_amount": 4500,
            "total_refunded": 0,
            "total_refunded_amount": 0
        }
    }
}
```
<div id="execution-results-GETevent--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETevent--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETevent--id-"></code></pre>
</div>
<div id="execution-error-GETevent--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETevent--id-"></code></pre>
</div>
<form id="form-GETevent--id-" data-method="GET" data-path="event/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETevent--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETevent--id-" onclick="tryItOut('GETevent--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETevent--id-" onclick="cancelTryOut('GETevent--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETevent--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>event/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETevent--id-" data-component="url" required  hidden>
<br>

</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETevent--id-" data-component="query" required  hidden>
<br>
the uuid of the event
</p>
</form>


## Get all the events


This endpoint is used to get a list of all upcoming events

> Example request:

```bash
curl -X GET \
    -G "http://localhost/events" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/events"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/events',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/events'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json

{
 "status": "success",
 "message": "Successfully got all the upcoming events",
 "events": [
     {
         "id": "3a8ba813-c754-4e98-814c-f707f44ac919",
         "title": "test",
         "image_path": "data/events/f0e25e92-8b29-4e3e-83e7-692317ce5411/images/6550467992d1bd36e639.png",
         "date": 1626510686
     },
 ]
}
```
<div id="execution-results-GETevents" hidden>
    <blockquote>Received response<span id="execution-response-status-GETevents"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETevents"></code></pre>
</div>
<div id="execution-error-GETevents" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETevents"></code></pre>
</div>
<form id="form-GETevents" data-method="GET" data-path="events" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETevents', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETevents" onclick="tryItOut('GETevents');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETevents" onclick="cancelTryOut('GETevents');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETevents" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>events</code></b>
</p>
</form>



