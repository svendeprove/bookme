<?php

namespace App\Helpers;

use App\Models\PaymentService;
use App\Models\PaymentType;
use App\Models\Ticket;
use App\Models\TicketType;
use Illuminate\Database\Eloquent\Model;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalHttp\HttpException;

class PaypalHelper
{

    public function validateAuth($paymentType, $companyId, $client, $secret){

            $environment = new SandboxEnvironment($client, $secret);
            $paypalClient = new PayPalHttpClient($environment);

            $request = new OrdersGetRequest("validate");

            try {
                $paypalClient->execute($request);

            }catch (HttpException $exception){
                if(str_contains($exception->getMessage(), "Authentication")){
                    return response()->json([
                        "status" => "success",
                        "message" => "Validation error of either the client or secret",
                    ], 400);
                }
            }

            $addPaymentService = new PaymentService();
            $addPaymentService->payment_type_id = $paymentType;
            $addPaymentService->company_id = $companyId;
            $addPaymentService->client_id = $client;
            $addPaymentService->secret_id = $secret;
            $addPaymentService->save();

            return response()->json([
                "status" => "success",
                "message" => "Successfully added the payment method to the company",
            ]);
    }

    public function createPayment($companyId, $amount, $ticketType, $email){

        $paymentType = PaymentType::where("name", "PAYPAL");

        if(!$paymentType->exists()){
            return response()->json([
                "status" => "error",
                "message" => "Error couldnt find the payment method",
            ], 400);
        }
        $paymentType = $paymentType->first()->id;

        $paymentService = PaymentService::where([["company_id", $companyId], ["payment_type_id", $paymentType]]);


        if(!$paymentService->exists()){
            return response()->json([
                "status" => "error",
                "message" => "Payment method not found",
            ], 404);
        }

        $paymentService = $paymentService->first();

        $environment = new SandboxEnvironment($paymentService->client_id, $paymentService->secret_id);
        $client = new PayPalHttpClient($environment);

        $request = new OrdersCreateRequest();
        $request->prefer("return=representation");
        $request->body = $this->createPaymentBody($amount);

        $response = $client->execute($request);


        //TODO: REFACTOR LATER SO IT ONLY returns the success response inside here
        if($response->statusCode == 201){
            $ticketHelper = new TicketHelper();
            $ticketHelper->create($ticketType, $amount, $response->result->id,"PAYPAL", $email);
        }

        return response()->json([
            "status" => "success",
            "message" => "The payment was successfully created",
            "order" => $response
        ]);

    }

    public function verify($orderId){

        $ticket = Ticket::where("order_id", $orderId)->with("ticketType")->with("account")->with("event");

        if(!$ticket->exists()){
            return response()->json([
                "status" => "error",
                "message" => "Something went wrong, please contact the support"
            ], 400);
        }

        $paymentType = PaymentType::where("name", "PAYPAL");

        if(!$paymentType->exists()){
            return response()->json([
                "status" => "error",
                "message" => "Error couldnt find the payment method",
            ], 400);
        }

        $companyId = $ticket->first()->event->company_id;
        $paymentType = $paymentType->first()->id;

        $paymentService = PaymentService::where([["company_id", $companyId], ["payment_type_id", $paymentType]]);

        if(!$paymentService->exists()){
            return response()->json([
                "status" => "error",
                "message" => "Payment method not found",
            ], 404);
        }

        $paymentService = $paymentService->first();

        $environment = new SandboxEnvironment($paymentService->client_id, $paymentService->secret_id);
        $client = new PayPalHttpClient($environment);


        $request = new OrdersGetRequest($orderId);
        try {
            $response = $client->execute($request);
            $paymentStatus = $response->result->status;
            $paymentTotalAmount = 0;

            foreach ($response->result->purchase_units as $item){
                $paymentTotalAmount += (double)$item->amount->value;
            }

            if($paymentStatus === "APPROVED") {

                $completeOrder = new OrdersCaptureRequest($orderId);
                $completeOrderResponse = $client->execute($completeOrder);

                if($completeOrderResponse->statusCode == "200" || $completeOrderResponse->statusCode == "201"){
                    $ticket->update(['status_id' => 1]);

                    $ticket = $ticket->first();
                    $pdfHelper = new PdfHelper();
                    $pdfHelper->generate($ticket);

                    $mail = new \App\Helpers\Mailhelper();
                    $mail->sendTicket($ticket->email, "Mail angående din billet", $ticket->id);

                }

                return response()->json([
                    "status" => "success",
                    "message" => "The order has been verified"
                ]);

            }else if($paymentStatus === "COMPLETED"){

                return response()->json([
                    "status" => "success",
                    "message" => "The order has already been verified"
                ]);

            } else{
                return response()->json([
                    "status" => "error",
                    "message" => "The payment failed, please contact our support. (". $paymentStatus. ")"
                ], 400);
            }

        }catch (HttpException $ex) {

            return response()->json([
                "status" => "error",
                "message" => "Something went wrong, couldnt verify the order!"
            ], $ex->statusCode);
        }
    }

    private function createPaymentBody($amount)
    {

        return array(
            'intent' => 'CAPTURE',
            'purchase_units' =>
                array(
                    0 =>
                        array(
                            'amount' =>
                                array(
                                    'currency_code' => 'DKK',
                                    'value' => $amount
                                )
                        )
                )
        );
    }

}
