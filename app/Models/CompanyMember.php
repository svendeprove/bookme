<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyMember extends Model
{
    public $timestamps = false;

    public function companyRole(){
        return $this->belongsTo(CompanyRole::class, "id", "role_id");
    }
}
