<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $accountUuid = Uuid::uuid4();

        $adminAccount = new Account();
        $adminAccount->id = $accountUuid;
        $adminAccount->firstName = "Admin";
        $adminAccount->lastName = "Admin";
        $adminAccount->email = "admin@bookme.dk";
        $adminAccount->password = password_hash("password", PASSWORD_BCRYPT);
        $adminAccount->save();

        $getAdminAccount = Account::where("id", $accountUuid)->first();
        $getAdminAccount->assignRole("Admin");

    }
}
