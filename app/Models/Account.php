<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Account extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory, HasRoles;

    protected $keyType = 'string';
    public $timestamps = false;

    protected $hidden = ["email", "password"];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }

    public function companies()
    {
        //   return $this->hasManyThrough(Ticket::class, TicketType::class, "event_id", "ticket_type_id", "id", "id");
        return $this->hasManyThrough(Company::class, CompanyMember::class, "account_id", "id", "id", "company_id")->select(["companies.id", "companies.name", "company_members.role_id"]);
    }

    public function companyEvents(){
        return $this->hasManyThrough(Event::class, CompanyMember::class, "account_id", "company_id", "id", "company_id")->select(["events.id", "events.title", "company_members.company_id"]);
    }


}
