<?php

namespace Database\Seeders;

use App\Models\PaymentType;
use App\Models\TicketStatus;
use Illuminate\Database\Seeder;

class PaymentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = array(
            array("name" => "PAYPAL"));

        PaymentType::insert($types);
    }
}
