# TicketType


## Create a Ticket Type

<small class="badge badge-darkred">requires authentication</small>

This endpoint is used to create a ticket type
Role required:
Admin
Event-Manager

> Example request:

```bash
curl -X POST \
    "http://localhost/ticket/type/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE" \
    -d '{"event_id":"autem","name":"hic","desc":"non","price":2,"available":10}'

```

```javascript
const url = new URL(
    "http://localhost/ticket/type/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE",
};

let body = {
    "event_id": "autem",
    "name": "hic",
    "desc": "non",
    "price": 2,
    "available": 10
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/ticket/type/create',
    [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE',
        ],
        'json' => [
            'event_id' => 'autem',
            'name' => 'hic',
            'desc' => 'non',
            'price' => 2,
            'available' => 10,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/ticket/type/create'
payload = {
    "event_id": "autem",
    "name": "hic",
    "desc": "non",
    "price": 2,
    "available": 10
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The ticket type has been created successfully"
}
```
<div id="execution-results-POSTticket-type-create" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTticket-type-create"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTticket-type-create"></code></pre>
</div>
<div id="execution-error-POSTticket-type-create" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTticket-type-create"></code></pre>
</div>
<form id="form-POSTticket-type-create" data-method="POST" data-path="ticket/type/create" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json","Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE"}' onsubmit="event.preventDefault(); executeTryOut('POSTticket-type-create', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTticket-type-create" onclick="tryItOut('POSTticket-type-create');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTticket-type-create" onclick="cancelTryOut('POSTticket-type-create');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTticket-type-create" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>ticket/type/create</code></b>
</p>
<p>
<label id="auth-POSTticket-type-create" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTticket-type-create" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>event_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="event_id" data-endpoint="POSTticket-type-create" data-component="body" required  hidden>
<br>
the id of the event
</p>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTticket-type-create" data-component="body" required  hidden>
<br>
the name of the ticket type
</p>
<p>
<b><code>desc</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="desc" data-endpoint="POSTticket-type-create" data-component="body" required  hidden>
<br>
a short description of the ticket type
</p>
<p>
<b><code>price</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="price" data-endpoint="POSTticket-type-create" data-component="body" required  hidden>
<br>
the price of the ticket type
</p>
<p>
<b><code>available</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="available" data-endpoint="POSTticket-type-create" data-component="body"  hidden>
<br>
how many of the ticket types that are available to be sold
</p>

</form>


## Delete a Ticket Type

<small class="badge badge-darkred">requires authentication</small>

This endpoint is used to delete a ticket type (can only be deleted if no tickets of this type, has been sold)
Role required:
Admin
Event-Manager

> Example request:

```bash
curl -X DELETE \
    "http://localhost/ticket/type/delete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE" \
    -d '{"id":9,"event_id":"cumque"}'

```

```javascript
const url = new URL(
    "http://localhost/ticket/type/delete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE",
};

let body = {
    "id": 9,
    "event_id": "cumque"
}

fetch(url, {
    method: "DELETE",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/ticket/type/delete',
    [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE',
        ],
        'json' => [
            'id' => 9,
            'event_id' => 'cumque',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/ticket/type/delete'
payload = {
    "id": 9,
    "event_id": "cumque"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE'
}

response = requests.request('DELETE', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The ticket type has been deleted successfully"
}
```
<div id="execution-results-DELETEticket-type-delete" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEticket-type-delete"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEticket-type-delete"></code></pre>
</div>
<div id="execution-error-DELETEticket-type-delete" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEticket-type-delete"></code></pre>
</div>
<form id="form-DELETEticket-type-delete" data-method="DELETE" data-path="ticket/type/delete" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json","Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE"}' onsubmit="event.preventDefault(); executeTryOut('DELETEticket-type-delete', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEticket-type-delete" onclick="tryItOut('DELETEticket-type-delete');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEticket-type-delete" onclick="cancelTryOut('DELETEticket-type-delete');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEticket-type-delete" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>ticket/type/delete</code></b>
</p>
<p>
<label id="auth-DELETEticket-type-delete" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEticket-type-delete" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="id" data-endpoint="DELETEticket-type-delete" data-component="body" required  hidden>
<br>
the id of the ticket type
</p>
<p>
<b><code>event_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="event_id" data-endpoint="DELETEticket-type-delete" data-component="body" required  hidden>
<br>
the id of the event
</p>

</form>


## Update a Ticket Type

<small class="badge badge-darkred">requires authentication</small>

This endpoint is used to update an event
Role required:
Admin
Event-Manager

> Example request:

```bash
curl -X PATCH \
    "http://localhost/ticket/type/update" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE" \
    -F "id=et" \
    -F "event_id=6" \
    -F "title=temporibus" \
    -F "desc=eum" \
    -F "country=omnis" \
    -F "city=consectetur" \
    -F "address=eum" \
    -F "date=18" \
    -F "image=@C:\Users\hund35\AppData\Local\Temp\php7C.tmp"     -F "images[]=@C:\Users\hund35\AppData\Local\Temp\php7D.tmp" 
```

```javascript
const url = new URL(
    "http://localhost/ticket/type/update"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE",
};

const body = new FormData();
body.append('id', 'et');
body.append('event_id', '6');
body.append('title', 'temporibus');
body.append('desc', 'eum');
body.append('country', 'omnis');
body.append('city', 'consectetur');
body.append('address', 'eum');
body.append('date', '18');
body.append('image', document.querySelector('input[name="image"]').files[0]);
body.append('images[]', document.querySelector('input[name="images[]"]').files[0]);

fetch(url, {
    method: "PATCH",
    headers,
    body,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->patch(
    'http://localhost/ticket/type/update',
    [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE',
        ],
        'multipart' => [
            [
                'name' => 'id',
                'contents' => 'et'
            ],
            [
                'name' => 'event_id',
                'contents' => '6'
            ],
            [
                'name' => 'title',
                'contents' => 'temporibus'
            ],
            [
                'name' => 'desc',
                'contents' => 'eum'
            ],
            [
                'name' => 'country',
                'contents' => 'omnis'
            ],
            [
                'name' => 'city',
                'contents' => 'consectetur'
            ],
            [
                'name' => 'address',
                'contents' => 'eum'
            ],
            [
                'name' => 'date',
                'contents' => '18'
            ],
            [
                'name' => 'image',
                'contents' => fopen('C:\Users\hund35\AppData\Local\Temp\php7C.tmp', 'r')
            ],
            [
                'name' => 'images[]',
                'contents' => fopen('C:\Users\hund35\AppData\Local\Temp\php7D.tmp', 'r')
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/ticket/type/update'
files = {
  'image': open('C:\Users\hund35\AppData\Local\Temp\php7C.tmp', 'rb')  'images[]': open('C:\Users\hund35\AppData\Local\Temp\php7D.tmp', 'rb')
}
payload = {
    "id": "et",
    "event_id": 6,
    "title": "temporibus",
    "desc": "eum",
    "country": "omnis",
    "city": "consectetur",
    "address": "eum",
    "date": 18
}
headers = {
  'Content-Type': 'multipart/form-data',
  'Accept': 'application/json',
  'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE'
}

response = requests.request('PATCH', url, headers=headers, files=files, data=payload)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The ticket type has been updated successfully"
}
```
<div id="execution-results-PATCHticket-type-update" hidden>
    <blockquote>Received response<span id="execution-response-status-PATCHticket-type-update"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PATCHticket-type-update"></code></pre>
</div>
<div id="execution-error-PATCHticket-type-update" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PATCHticket-type-update"></code></pre>
</div>
<form id="form-PATCHticket-type-update" data-method="PATCH" data-path="ticket/type/update" data-authed="1" data-hasfiles="2" data-headers='{"Content-Type":"multipart\/form-data","Accept":"application\/json","Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FjY291bnRcL2xvZ2luIiwiaWF0IjoxNjIwODAzOTg3LCJleHAiOjE2MjA4NDcxODcsIm5iZiI6MTYyMDgwMzk4NywianRpIjoiSlhaTnZHU1FxZmZIZmRjNSIsInN1YiI6IjYwMGIwMWUxLTM0YzctNDI4OC1iNGQ3LTUyMjA1ZmQzMGY3YSIsInBydiI6ImM4ZWUxZmM4OWU3NzVlYzRjNzM4NjY3ZTViZTE3YTU5MGI2ZDQwZmMifQ.MqIt2uuiyiO5M-zdHC2p4aI0zkxqM3mfPSXzFWOxQuE"}' onsubmit="event.preventDefault(); executeTryOut('PATCHticket-type-update', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PATCHticket-type-update" onclick="tryItOut('PATCHticket-type-update');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PATCHticket-type-update" onclick="cancelTryOut('PATCHticket-type-update');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PATCHticket-type-update" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>ticket/type/update</code></b>
</p>
<p>
<label id="auth-PATCHticket-type-update" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PATCHticket-type-update" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="PATCHticket-type-update" data-component="body" required  hidden>
<br>
the id of the ticket type
</p>
<p>
<b><code>event_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="event_id" data-endpoint="PATCHticket-type-update" data-component="body" required  hidden>
<br>
the id of the event
</p>
<p>
<b><code>title</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="title" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<br>
the title of the event
</p>
<p>
<b><code>desc</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="desc" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<br>
the description of the event
</p>
<p>
<b><code>country</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="country" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<br>
the country of the event
</p>
<p>
<b><code>city</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="city" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<br>
the city of the event
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="address" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<br>
the address of the event
</p>
<p>
<b><code>date</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="date" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<br>
the date of the event
</p>
<p>
<b><code>image</code></b>&nbsp;&nbsp;<small>file</small>     <i>optional</i> &nbsp;
<input type="file" name="image" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<br>
the event image
</p>
<p>
<b><code>images</code></b>&nbsp;&nbsp;<small>file[]</small>     <i>optional</i> &nbsp;
<input type="file" name="images.0" data-endpoint="PATCHticket-type-update" data-component="body"  hidden>
<input type="file" name="images.1" data-endpoint="PATCHticket-type-update" data-component="body" hidden>
<br>
the event images
</p>

</form>


## Get all Ticket Types


This endpoint is used to get all the ticket types

> Example request:

```bash
curl -X GET \
    -G "http://localhost/ticket/types/veritatis?event_id=sapiente" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/ticket/types/veritatis"
);

let params = {
    "event_id": "sapiente",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/ticket/types/veritatis',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'query' => [
            'event_id'=> 'sapiente',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/ticket/types/veritatis'
params = {
  'event_id': 'sapiente',
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

response = requests.request('GET', url, headers=headers, params=params)
response.json()
```


> Example response (200):

```json
{
    "status": "success",
    "message": "The ticket types has been fetched successfully",
    "ticket_types": [
        {
            "id": 3,
            "event_id": "3a8ba813-c754-4e98-814c-f707f44ac919",
            "name": "VIP",
            "desc": "Super VIP ticket",
            "price": 2500,
            "available": 10
        }
    ]
}
```
<div id="execution-results-GETticket-types--eventId-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETticket-types--eventId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETticket-types--eventId-"></code></pre>
</div>
<div id="execution-error-GETticket-types--eventId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETticket-types--eventId-"></code></pre>
</div>
<form id="form-GETticket-types--eventId-" data-method="GET" data-path="ticket/types/{eventId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETticket-types--eventId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETticket-types--eventId-" onclick="tryItOut('GETticket-types--eventId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETticket-types--eventId-" onclick="cancelTryOut('GETticket-types--eventId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETticket-types--eventId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>ticket/types/{eventId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>eventId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="eventId" data-endpoint="GETticket-types--eventId-" data-component="url" required  hidden>
<br>

</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>event_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="event_id" data-endpoint="GETticket-types--eventId-" data-component="query" required  hidden>
<br>
the id of the event
</p>
</form>



